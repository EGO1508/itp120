
/**
 * Write a description of class Cat here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Cat extends Feline implements Pet
{
    // instance variables - replace the example below with your own
    public void eat() 
    {
        System.out.println("Eating a mouse");
    }
    
    public void makeNoise()
    {
       System.out.println("Meaow"); 
    }
    
    public void roam()
    {
        System.out.println("Roaming");
    }
    
    public void beFriendly() {
        System.out.println("Being Friendly");
    }
    
    public void play() {
        System.out.println("Play with toy mouse");
    }
}
