
/**
 * Write a description of class Dog here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Dog extends Canine implements Pet
{
    // instance variables - replace the example below with your own
    public void eat() 
    {
        System.out.println("Eating a bone");
    }
    
    public void makeNoise()
    {
       System.out.println("Woof"); 
    }
    
    public void roam()
    {
        System.out.println("Roaming");
    }
    
    public void beFriendly() {
        System.out.println("Being Friendly");
    }
    
    public void play() {
        System.out.println("Play fetch");
    }
}
