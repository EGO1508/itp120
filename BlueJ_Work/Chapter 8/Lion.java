
/**
 * Write a description of class Lion here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Lion extends Feline
{
    // instance variables - replace the example below with your own
    public void eat() 
    {
        System.out.println("Eating meat");
    }
    
    public void makeNoise()
    {
       System.out.println("Lion noises"); 
    }
    
    public void roam()
    {
        System.out.println("Roaming");
    }
}
