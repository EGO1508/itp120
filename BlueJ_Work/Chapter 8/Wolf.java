
/**
 * Write a description of class Wolf here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Wolf extends Canine
{
    // instance variables - replace the example below with your own
    public void eat() 
    {
        System.out.println("Eating meat");
    }
    
    public void makeNoise()
    {
       System.out.println("Wolf noises"); 
    }
    
    public void roam()
    {
        System.out.println("Roaming");
    }
}
