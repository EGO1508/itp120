
public abstract class Animal
{
    // instance variables - replace the example below with your own
    private String picture;
    private String food;
    private int hunger;
    private String boundaries;
    private String location;
    
    public abstract void eat();
    public abstract void roam();
    public abstract void makeNoise();
    public void sleep()
    {
        System.out.println("ZzzzzzZzzzzz");
    }    
    
}
