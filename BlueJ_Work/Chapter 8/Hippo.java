
/**
 * Write a description of class Hippo here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

public class Hippo extends Animal
{
    public void eat() 
    {
        System.out.println("Eating a fruit");
    }
    
    public void makeNoise()
    {
       System.out.println("Hippo noises"); 
    }
    
    public void roam()
    {
        System.out.println("Roaming");
    }
}
