
public class MyDogList
{
    // instance variables - replace the example below with your own
    private Dog [] dogs = new Dog[5];
    
    private int nextIndex = 0;
    
    public void add(Dog d) {
        if (nextIndex < dogs.length){
            dogs[nextIndex] = d;
            
            System.out.println("Dog added at " + nextIndex);
            nextIndex++;
        }
    }
}
