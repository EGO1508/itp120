
/**
 * Write a description of class MyAnimalList here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class MyAnimalList
{
    // instance variables - replace the example below with your own
    private Animal [] animals = new Animal[5];
    
    private int nextIndex = 0;
    
    public void add(Animal a) {
        if (nextIndex < animals.length){
            animals[nextIndex] = a;
            
            System.out.println("Animal added at " + nextIndex);
            nextIndex++;
        }
    }
}
