
/**
 * Write a description of class RobotDog here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RobotDog extends Robot implements Pet
{
    // instance variables - replace the example below with your own
    public void beFriendly() {
        System.out.println("Being Friendly");
    }
    
    public void play() {
        System.out.println("Play fetch");
    }
    
    public void makeNoise(){
        System.out.print("Make Robot Dog Noises");
    }
}
