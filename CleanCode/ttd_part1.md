1. 1 Write no production code except to pass a failing test. 
   2 Write only enough of a test to demonstrate a failure.
   3 Write only enough production code to pass the test. 
2. Write tests -> Think about how to make those test pass -> Improve your implementation
3. Code that is rigid, difficult to understand and difficult to modify
4. Fear exists because you know you need to clean the code but you also know that cleaning it is going to take a lot of time. TDD helps break this cycle because while cleaning it, you use the tests that you created to make shure the system works as intended
5. FitNeese is a testing tool. It tests the applications that the user is developing.
6. That its rigid and resists change. From the rigidity of the system.
7. It allows you to not frea the code, it allows you to change it with no fear of it breaking.