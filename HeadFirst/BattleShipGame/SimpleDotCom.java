class SimpleDotCom
{
    private int[] locations;
    private int hits = 0;
    private int guesses = 0;
    private boolean[] hitCells;

    public SimpleDotCom() {
        int start = (int)(Math.random() * 5);
        locations = new int[] {start, start + 1, start + 2};
        hitCells = new boolean[7];
    }

    public void setLocationCells(int[] setLocations) {
        locations = setLocations;
    }

    public boolean over() {
        return hits > 2;
    }

    public String makeMove(String userGuess) {
        int guess = Integer.parseInt(userGuess);
        guesses++;

        for (int location : locations) {
            if (guess == location && !hitCells[location]) {
                hits++;
                hitCells[location] = true; 
                if (hits > 2)
                    return "kill";
                return "hit";
            }
        }
        return "miss";
    }

    public String displayResults() {
        return "You took " + guesses + " guesses.";
    }
}
