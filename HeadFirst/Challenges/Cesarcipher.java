import java.io.*;
import java.util.*;

public class Cesarcipher {
	
	public static String encrypt(String plainText, int shift) {
		
		if(shift>26) {		// The number cannot be greater than 26 or less than 0
			shift = 0;
		}
		else if (shift<0) {
			shift = 0;
		}
		
		String enterText = "";
		int length = plainText.length();
		
		for(int i = 0; i<length; i++) { 
			char ch = plainText.charAt(i);
			if(Character.isLetter(ch)) {
				if(Character.isLowerCase(ch)) {
					char c = (char)(ch+shift);
					if(c>'z') {
						enterText += (char)(ch - (26-shift));	// This loops the number back to the beginning of the alphabet if it reaches the end
					}
					else {
						enterText += c;
					}
				}
				else if(Character.isUpperCase(ch)) {	// Same process for uppercase characters
					char c = (char)(ch+shift);
					if(c>'Z') {
						enterText += (char)(ch - (26-shift));
					}
					else {
						enterText += c;
					}
				}
			}
			else {
				enterText += ch;
			}
		}

		return enterText;
	}

	public static void main(String[] args) {
	
		Scanner scanner = new Scanner(System.in);
		int onLine = 0;
		int numberShift = 0;
		String encryptThis = "";		// Enter text here
		Boolean stop = true;

		while (scanner.hasNextLine() && stop) {
			String line = scanner.nextLine();
			onLine++;

			if (onLine == 1) {
				numberShift = Integer.parseInt(line);
			}else if (line.contains("STOP")) {
				stop = false;
			}else{
				encryptThis += line;
			}
			
		}
		
		String cipher = encrypt(encryptThis, numberShift);	// Enter shift number here
		System.out.println(cipher);

	}

}
