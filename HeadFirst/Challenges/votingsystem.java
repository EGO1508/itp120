import java.util.*;
import java.io.*;

public class votingsystem {


    public static void main(String[] args) throws IOException {

        Scanner voteList = new Scanner(System.in);        // Scans the text file to run the test

        String[] theChar = {"","",""};

        int[] temp = {0,0,0};
        int tallyOne = 0;
        int tallyTwo = 0;
        int tallyThree = 0;
        int election = 0;
        
        
        while(voteList.hasNextLine()) {
            String line = voteList.nextLine();

            theChar[0] = line.substring(0,2);    // Grab the first 2 things on the line
            theChar[0] = theChar[0].replaceAll("\\s", "");    // Remove white space
            temp[0] = Integer.parseInt(theChar[0]);     // Make it into an integer

            if (temp[0] == 0) {
                election++;

                System.out.println("election " + election);
                System.out.println(tallyOne + " votes for candidate 1");
                System.out.println(tallyTwo + " votes for candidate 2");
                System.out.println(tallyThree + " votes for candidate 3");

                tallyOne = 0;
                tallyTwo = 0;
                tallyThree = 0;
            }else if (temp[0] == -1) {
                election++;
                System.out.println("--------- election " + election);
                System.out.println(tallyOne + " votes for candidate 1");
                System.out.println(tallyTwo + " votes for candidate 2");
                System.out.println(tallyThree + " votes for candidate 3");
                System.out.println("No more votes");
            }else if (temp[0] == 1) {            // Add a vote for each candidate
                tallyOne++;
            }else if (temp[0] == 2) {
                tallyTwo++;
            }else if (temp[0] == 3) {
                tallyThree++;
            }
        }
    }
}