import java.io.*;
import java.util.*;

class Registration {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        // Creates an array that stores the player for each house
        String[] allHouses = {
            "Griffindor", "Slytherin", "Hufflepuf", "Ravenclaw"
        };
        String[] letterOfHouse = {"G","S","H","R"};
        // Creates an array of all of the houses
        int[] playersPerHouse = {0, 0, 0, 0};
        int fullRoster = 0;
        int lines = 0;

        while (scanner.hasNextLine()) {
            String house = scanner.nextLine();



            for (int i = 1; i > 0; i--) {
                // Removes everything before coma and white space
                String finalHouse = house.substring(
                    house.lastIndexOf(", ") + 1
                );
                // Grabs the first letter of the String thats left
                String firstLetter = finalHouse.substring(1,2);

                // This code increases the value of each house everytime a new
                // player is detected

                if (firstLetter.equals(letterOfHouse[0])) {
                    playersPerHouse[0]++;
                } else if (firstLetter.equals(letterOfHouse[1])) {
                    playersPerHouse[1]++;
                } else if (firstLetter.equals(letterOfHouse[2])) {
                    playersPerHouse[2]++;
                } else if (firstLetter.equals(letterOfHouse[3])) {
                    playersPerHouse[3]++;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
                // This code checks the value each house to know how many
                // players there are in that house 
            if (playersPerHouse[i] < 7){
                System.out.println(
                    "Team " + allHouses[i] + " doesnt have enough players"
                );
            } else if (playersPerHouse[i] > 7) {
                System.out.println(
                    "Team " + allHouses[i] + " has to many players"
                );
            } else if (playersPerHouse[i] == 7){
                fullRoster++;
            }
        }
        if (fullRoster == 4) {
                // If the player count is 7, full Roster gains 1. When full
                // roster is 4, it prints ready message
            System.out.println("We are ready to play");
        }
    }
}